
nnoremap <plug>openManPage :execute 'Man '.viman#openManPage(0)<cr>
vnoremap <plug>openManPage :execute 'Man '.viman#openManPage(1)<cr>

if (&filetype ==# 'man')
	nmap <silent> <buffer> K <plug>openManPage
	vmap <silent> <buffer> K y:Man <c-r>"<cr>

	" like popping tag (go to previous help)
	nnoremap <buffer> <silent> <c-t> :call viman#back()<cr>

	" reflow buffer
	nnoremap <buffer> <silent> <c-r> :execute 'Man '.viman#getCManPage()<cr>

	call viman#enter()
endif

