
" load man.vim from vimruntime if :Man doesn't yet exist
if (!exists(':Man'))
	source $VIMRUNTIME/ftplugin/man.vim
endif

