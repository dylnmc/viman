# ViMan

An easy solution for browsing man pages in ViM.

## Setup and Installation

### Setting Up Basrc or Zshrc

You will need to add this to your bash or zsh rc file in order to be able to type `man {ManPage}` from the command line:

``` bash
function man(){
	if ! [[ -t 0 && -t 1 && -t 2 ]]; then
		command man "$@"
		return
	fi;
	if [ $# -eq 0 ]; then
		echo 'What manual page do you want?'
		return 0
	elif ! man -d "$@" &> /dev/null; then
		echo 'No manual entry for '"$*"
		return 1
	fi;
	vim +'Man '"$*" +'wincmd o'
}
```

### Installing

`let no_man_maps = 1` will disable the horrendous maps that come with `$VIMRUNTIME/ftplugin/man.vim`. In the future, you will not need to do this. For now, it is a necessary evil.

In order to add the gitlab repository, you need to do the following in order:
1. `let g:plug_url_format = 'https://git::@gitlab.com/%s.git`
2. `Plug 'dylnmc/ViMan'`
3. `unlet g:plug_url_format`

### Example Minimal Vimrc

``` diff
  set nocompatible

" vim 'options'

+ let no_man_maps = 1

  call plug#begin('~/.vim/plugged')

+ let g:plug_url_format = 'https://git::@gitlab.com/%s.git'
+ Plug 'dylnmc/ViMan'
+ unlet g:plug_url_format

  " Other github vim plugins
```

## Usage

### Command Line

The purpose of this project is to seemlessly replace the old man pager with vim

``` bash
$ man {ManPage}
```

### Vim

* You will now always have access to the `:Man` command. For example: `:Man {ManPage}`
  - This can understand sections. For example: `:Man 3 printf`

* <kbd>Shift</kbd>+<kbd>K</kbd> will parse the text under the cursor and open the man page if it exists.
  - This is able to understand sections
  - For example pressing <kbd>Shift</kbd>+<kbd>K</kbd> on `printf(3)` will open
    `3 printf`

* If the screen size changes, you can re-flow the text with
  <kbd>Ctrl</kbd>+<kbd>R</kbd>

## Licence

This project is licensed under the GPL License. See the [LICENSE.md](LICENSE.md) file for details.

## TODO

1. Don't use `$VIMRUNTIME/ftplugin/man.vim` because it's terrible
  - This means creating own `:Man` command and filling in all necessary bugs
