
function! viman#getCManPage()
	let l:f = expand('%:t')
	if (&filetype !=# 'man' || l:f[len(l:f) - 1] !=# '~')
		return ''
	endif
	return join(reverse(split(l:f[:-2], '\.')), ' ')
endfunction

function! viman#removeAll(list, expr)
	let l:ind = 0
	for l:item in a:list
		if l:item ==# a:expr
			call remove(a:list, l:ind)
		else
			let l:ind += 1
		endif
	endfor
endfunction

function! viman#back()
	if (len(get(g:, 'manPages', [])) <= 1)
		echohl ErrorMsg
		echon 'There are no more previous man pages.'
		echohl NONE
		return
	endif
	let l:cManPage = viman#getCManPage()
	if empty(l:cManPage)
		return
	endif
	call viman#removeAll(g:manPages, l:cManPage)
	execute 'Man '.g:manPages[-1]
endfunction

function! viman#enter()
	if empty(get(g:, 'manPages', []))
		let g:manPages = []
	endif
	let l:cManPage = viman#getCManPage()
	if empty(l:cManPage)
		return
	endif
	call viman#removeAll(g:manPages, l:cManPage)
	call add(g:manPages, l:cManPage)
endfunction

function! viman#openManPage(visual)
	let l:aSave = @a
	if (!a:visual)
		let l:line = line('.')
		call search('\m\<', 'bc', l:line)
		normal! v
		call search('\C\v%(\w|-|\.)+(\(%([1-9npx]|1M)\))?', 'ec', l:line)
	endif
	normal! "ay
	let l:ret = @a
	let @a = l:aSave
	if (l:ret =~# '\m(..\?)$')
		let l:sec = substitute(l:ret, '\m.*(\(..\?\))$', '\1', '')
		return l:sec.' '.substitute(l:ret, '\m\(.*\)(..\?)$', '\1', '')
	endif
	return l:ret
endfunction


" " TODO ...
" function! viman#getPage(cmdmods, ...)
" 	if a:0 >= 2
" 		let sect = a:1
" 		let page = a:2
" 	elseif a:0 >= 1
" 		let sect = ""
" 		let page = a:1
" 	else
" 		return
" 	endif
" 
" 	if sect != "" && s:FindPage(sect, page) == 0
" 		let sect = ""
" 	endif
" 	if s:FindPage(sect, page) == 0
" 		echo "\nCannot find a '".page."'."
" 		return
" 	endif
" 	exec "let s:man_tag_buf_".s:man_tag_depth." = ".bufnr("%")
" 	exec "let s:man_tag_lin_".s:man_tag_depth." = ".line(".")
" 	exec "let s:man_tag_col_".s:man_tag_depth." = ".col(".")
" 	let s:man_tag_depth = s:man_tag_depth + 1
" 
" 	" Use an existing "man" window if it exists, otherwise open a new one.
" 	if &filetype != "man"
" 		let thiswin = winnr()
" 		exe "norm! \<C-W>b"
" 		if winnr() > 1
" 			exe "norm! " . thiswin . "\<C-W>w"
" 			while 1
" 				if &filetype == "man"
" 					break
" 				endif
" 				exe "norm! \<C-W>w"
" 				if thiswin == winnr()
" 					break
" 				endif
" 			endwhile
" 		endif
" 		if &filetype != "man"
" 			if exists("g:ft_man_open_mode")
" 				if g:ft_man_open_mode == "vert"
" 					vnew
" 				elseif g:ft_man_open_mode == "tab"
" 					tabnew
" 				else
" 					new
" 				endif
" 			else
" 				if a:cmdmods != ''
" 					exe a:cmdmods . ' new'
" 				else
" 					new
" 				endif
" 			endif
" 			setl nonu fdc=0
" 		endif
" 	endif
" 	silent exec "edit $HOME/".page.".".sect."~"
" 	" Avoid warning for editing the dummy file twice
" 	setl buftype=nofile noswapfile
" 
" 	setl ma nonu nornu nofen
" 	silent exec "norm! 1GdG"
" 	let unsetwidth = 0
" 	if empty($MANWIDTH)
" 		let $MANWIDTH = winwidth(0)
" 		let unsetwidth = 1
" 	endif
" 
" 	" Ensure Vim is not recursively invoked (man-db does this) when doing ctrl-[
" 	" on a man page reference by unsetting MANPAGER.
" 	" Some versions of env(1) do not support the '-u' option, and in such case
" 	" we set MANPAGER=cat.
" 	if !exists('s:env_has_u')
" 		call system('env -u x true')
" 		let s:env_has_u = (v:shell_error == 0)
" 	endif
" 	let env_cmd = s:env_has_u ? 'env -u MANPAGER' : 'env MANPAGER=cat'
" 	let man_cmd = env_cmd . ' man ' . s:GetCmdArg(sect, page) . ' | col -b'
" 	silent exec "r !" . man_cmd
" 
" 	if unsetwidth
" 		let $MANWIDTH = ''
" 	endif
" 	" Remove blank lines from top and bottom.
" 	while getline(1) =~ '^\s*$'
" 		silent keepj norm! ggdd
" 	endwhile
" 	while getline('$') =~ '^\s*$'
" 		silent keepj norm! Gdd
" 	endwhile
" 	1
" 	setl ft=man nomod
" 	setl bufhidden=hide
" 	setl nobuflisted
" 	setl noma
" endfunc

